# Event Analysers

This is a small set of tools analysing Kubernetes/OpenShift Events.

## BadPodClassifier

This on will generate metrics on bad pods... 

This application will consume events via Kafka Topic `openshift.raw` on broker `kafka:9092`. All events related to Pods will be analysed.

Analyzed data is privided as Prometheus metrics.

### Prerequisites

* have Kafka up and running, use `oc process --param=IMAGE_REPO_NAME=goern --labels=app=kafka -f https://raw.githubusercontent.com/EnMasseProject/barnabas/master/kafka-statefulsets/resources/openshift-template.yaml | oc create -f -` to do so
* have eventrouter publish events to Kafka, use `oc process --labels=app=observers -f https://gitlab.com/goern/eventrouter/raw/master/openshift-template.yaml | oc create -f -` to do so

#### Deploy Prometheus on OpenShift

```
oc login -u system:admin
oc process -f https://raw.githubusercontent.com/openshift/origin/master/examples/prometheus/prometheus.yaml | oc create -f -
oc adm policy add-cluster-role-to-user view system:anonymous # THIS IS A SECURITY ISSUE
oc login -u developer
```

And dont forget to grant priv to see all pods in all namespaces via `oc adm policy add-cluster-role-to-user view system:serviceaccount:analytics:default`.

### Deploy

```
oc new-app --labels=app=analysers --env=KAFKA_PEERS=kafka:9092 https://gitlab.com/openshift-analytics/raw-events-switchyard.git
oc expose service raw-events-switchyard
```

Use curl to test it:

```
curl -v `oc get route raw-events-switchyard --template='{{.spec.host}}'`/metrics
```

If you tell Prometheus to scrape metrics from this route, you will be able to use `openshift_analytics_pod_events` Histogram...

To configure a scraper for bad-pod service add something like the below snippet to Prometheu's configmap: `oc edit configmap --namespace=kube-system prometheus`

```
    - job_name: 'openshift-analytics'
      static_configs:
        - targets: ['raw-events-switchyard.analytics.svc:8080']

    - job_name: 'eventrouter'
      static_configs:
        - targets: ['eventrouter.analytics.svc:8080']
```

Afterwards restart prometheus: `oc delete pod --namespace=kube-system --selector=app=prometheus`

As OpenShift restarts the Pod for us...

![Prometheus Overview](graphics/Overview.png "Prometheus Metrics Overview")

Especially useful is a look at for example the openshift_analytics_pod_events_count for label:reason, we could figure out that pods were not about to mount some volume, this could mean that someone forgot to provide a config map...

![Prometheus FailedMount](graphics/FailedMount.png "Prometheus Metrics FailedMount")

Another thing we could see is broken deployment configuration. Assuming we have a deployment configuration that needs some config map, and assuming that the config map has not been created, we could have a Prometheus query like `round(sum(irate(openshift_analytics_pod_events_bucket{reason="FailedMount"}[5m])) by (reason))` and see that whenever this hits 1, there is a Pod unable to mount something, this might be our ConfigMap (or any other volume...).