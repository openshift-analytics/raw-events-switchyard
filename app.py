#!/usr/bin/env python

"""EventAnalyser.py: This will classify Kubernetes/OpenShift Events.
 - count: The good, the bad, the failed!
 - route: Builds, Deployments
"""

__version__ = '0.2.0'

from flask import Flask, jsonify
from prometheus_client import generate_latest, REGISTRY, Counter, Histogram, Summary
from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import KafkaError, NoBrokersAvailable
from threading import Thread

from EventStreamProcessor import *

import json
import logging
import os
import urllib3


app = Flask(__name__)

# Lets get our config from ENV
# Application
DEBUG_LOG_LEVEL = bool(os.getenv('DEBUG', False))
# OpenShift
OPENSHIFT_API_ENDPOINT = 'https://127.0.0.1:8443/oapi/v1/'
BEARER_TOKEN = os.getenv('BEARER_TOKEN', 'UNSET')
# Kafka
BOOTSTRAP_SERVERS = os.getenv('KAFKA_PEERS', 'localhost:9092')
CONSUMED_TOPIC = os.getenv('CONSUMED_TOPIC', RAW_TOPIC_NAME)

# These Pod States are considered 'not bad'
okReasons = ["Pulling", "Pulled", "Scheduled", "Created",
             "SuccessfulMountVolume", "Started", "Killing"]

# All the Counters for 'bad' Pod events
TOTAL_PODS_EVENTS = Histogram('openshift_analytics_pod_events',
                              'Observed and analysed OpenShift Events',
                              ['reason'])
TOTAL_BUILD_EVENTS = Counter('openshift_analytics_build_events_total',
                             'Observed OpenShift Build Events')
TOTAL_DEPLOYMENT_EVENTS = Counter('openshift_analytics_deployment_events_total',
                                  'Observed OpenShift Deployment Events')


class RawStreamProcessor(EventStreamProcessorAndOpenShiftClient, Thread):
    daemon = True

    def __init__(self, endpoint, token, topic, bootstrap_servers,
                 group=None, target=None, name=None, args=(), kwargs=None):
        super(RawStreamProcessor, self).__init__(
            endpoint, token, topic, bootstrap_servers)
        Thread.__init__(self, group=group, target=target, name=name)

        self.producer = KafkaProducer(bootstrap_servers=self.bootstrap_servers,
                                      value_serializer=lambda m: json.dumps(
                                          m).encode('utf-8'),
                                      retries=5)

        self.classlogger = logging.getLogger('RawStreamProcessor')

    def emitPodEvent(self, pod_event):
        obj = self.fetch("namespaces/%s/pods/%s" %
                         (pod_event['involvedObject']['namespace'], pod_event['involvedObject']['name']))

        if obj != None:
            try:
                _obj = json.loads("".join(obj))
            except json.JSONDecodeError as e:
                self.classlogger.error(e)
                return None

            self.producer.send(PODS_OBJECTS_TOPIC_NAME, _obj)

            return _obj

        return None

    def emitDeploymentEvent(self, deployment_event):
        self.sendToTopic(DEPLOYMENTS_DISCOVERED_TOPIC_NAME, deployment_event)

        obj = self.fetch("namespaces/%s/deploymentconfigs/%s" %
                         (deployment_event['involvedObject']['namespace'], deployment_event['involvedObject']['name']))

        if obj != None:
            try:
                _obj = json.loads("".join(obj))
            except json.JSONDecodeError as e:
                self.classlogger.error(e)
                return None

            self.producer.send(DEPLOYMENTS_OBJECTS_TOPIC_NAME, _obj)

            return _obj

        return None

    def emitBuildEvent(self, build_event):
        # transform it so that git-enricher can handle the event
        build = {}
        build['name'] = build_event['involvedObject']['name']
        build['uid'] = build_event['involvedObject']['uid']
        build['namespace'] = build_event['involvedObject']['namespace']
        build['created_at'] = build_event['metadata']['creationTimestamp']
        build['event_id'] = build_event['metadata']['uid']

        if build_event['message'].endswith("is now running"):
            build['start'] = build_event['metadata']['creationTimestamp']
        elif build_event['message'].endswith("completed successfully"):
            build['end'] = build_event['metadata']['creationTimestamp']

        # send if to the topic
        self.sendToTopic(BUILDS_DISCOVERED_TOPIC_NAME, build)
        logger.debug("%s", build)

    def sendToTopic(self, topic, event):
        self.producer.send(topic, event)

    def run(self):
        try:
            self.consumer = KafkaConsumer(RAW_TOPIC_NAME,  # TODO chk if this is really threadsafe
                                          value_deserializer=lambda m: json.loads(
                                              m.decode('utf-8')),
                                          bootstrap_servers=BOOTSTRAP_SERVERS)

            self.producer = KafkaProducer(
                value_serializer=lambda v: json.dumps(v).encode('utf-8'),
                bootstrap_servers=self.bootstrap_servers)
        except NoBrokersAvailable as e:
            logger.error(e)
            return

        for message in self.consumer:
            logger.debug("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                                        message.offset, message.key,
                                                        message.value))

            # we completely ignore old_event in case of an UPDATE verb
            event = message.value['event']

            # lets get all the pods that didnt start...
            if event['involvedObject']['kind'] == 'Pod':
                self.emitPodEvent(event)

                # This is an OpenShift <3.6 build pod
                if event['involvedObject']['name'].endswith('-build'):
                    self.emitBuildEvent(event)
                    TOTAL_BUILD_EVENTS.inc()

                if event['reason'] not in okReasons:
                    TOTAL_PODS_EVENTS.labels('Bad').observe(1)

                    if event['reason'] == 'FailedMount':
                        TOTAL_PODS_EVENTS.labels('FailedMount').observe(1)

                    elif event['reason'] == 'FailedSync':
                        TOTAL_PODS_EVENTS.labels('FailedSync').observe(1)
                        continue

                    elif event['reason'] == 'BackOff':
                        TOTAL_PODS_EVENTS.labels('BackOff').observe(1)

                    elif event['reason'] == 'FailedScheduling':
                        TOTAL_PODS_EVENTS.labels('FailedScheduling').observe(1)
                        continue

                    elif event['reason'] == 'Failed':
                        TOTAL_PODS_EVENTS.labels('Failed').observe(1)

                    logger.debug("Pod: %s: %s, Message: %s" % (
                        event['involvedObject']['name'], event['reason'], event['message']))
                else:
                    TOTAL_PODS_EVENTS.labels('Ok').observe(1)

            # This is an OpenShift 3.6+ Build
            elif event['involvedObject']['kind'] == 'Build':
                self.emitBuildEvent(event)
                TOTAL_BUILD_EVENTS.inc()

            elif event['involvedObject']['kind'] == 'DeploymentConfig':
                self.emitDeploymentEvent(event)
                TOTAL_DEPLOYMENT_EVENTS.inc()


@app.route('/')
def hello_world():
    return 'There is only /metrics in here!'


@app.route('/metrics')
def metrics():
    return generate_latest(REGISTRY)


@app.route('/_healthz')
def healthz_liveliness():
    liveliness = {
        'rawStreamProcessor': rawStreamProcessor.is_alive(),
    }

    if rawStreamProcessor.is_alive():
        liveliness['OK'] = True
    else:
        liveliness['OK'] = False

    return jsonify(liveliness), liveliness['OK']


@app.route('/_healthz/readyness')
def healthz_readyness():
    if rawStreamProcessor.is_alive():
        return jsonify('READY'), 200
    else:
        return jsonify("NOTREADY"), 399

if __name__ == "__main__":
    """The main function
    This will launch the main Spark/Kafka stream processor class.
    """

    if DEBUG_LOG_LEVEL:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
        urllib3.disable_warnings()

    logger = logging.getLogger(__name__)

    logger.info('initializing...')

    # if we can read bearer token from /var/run/secrets/kubernetes.io/serviceaccount/token use it,
    # otherwise use the one from env
    try:
        logger.debug(
            'trying to get bearer token from secrets file within pod...')
        with open('/var/run/secrets/kubernetes.io/serviceaccount/token') as f:
            BEARER_TOKEN = f.read()

        # if we can read bearer token from file, use openshift.svc as anedpoint
        # otherwise use localhost
        OPENSHIFT_API_ENDPOINT = 'https://openshift.default.svc.cluster.local/oapi/v1/'

    except:
        logger.info("not running within an OpenShift cluster...")

    logger.debug("Using %s and Bearer token %s" %
                 (OPENSHIFT_API_ENDPOINT, BEARER_TOKEN))

    rawStreamProcessor = RawStreamProcessor(
        OPENSHIFT_API_ENDPOINT, BEARER_TOKEN, topic=CONSUMED_TOPIC, bootstrap_servers=BOOTSTRAP_SERVERS)

    rawStreamProcessor.start()

    app.run(host='0.0.0.0', port=8080, debug=DEBUG_LOG_LEVEL)
