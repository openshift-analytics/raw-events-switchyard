#!/bin/env python

from kafka import KafkaProducer
from kafka.errors import KafkaError

import json
import logging
import os

producer = KafkaProducer(bootstrap_servers="localhost:9092",
                                      value_serializer=lambda m: json.dumps(
                                          m).encode('ascii'),
                                      retries=5)


with open('fixtures/some-dump.json') as fixture:
    events = json.load(fixture)

    for event in events:
        producer.send('openshift.raw', event)
        
    producer.flush()